﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marshalls.Application.Dtos.Models
{
    public class EmployeeBonoDto
    {
        public string Code { get; set; }
        public string FullName { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public decimal Salary1 { get; set; }
        public decimal Salary2 { get; set; }
        public decimal Salary3 { get; set; }
        public decimal Bono { get; set; }
    }
}
