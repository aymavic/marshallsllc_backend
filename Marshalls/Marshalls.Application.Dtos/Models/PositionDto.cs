﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marshalls.Application.Dtos.Models
{
    public class PositionDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
