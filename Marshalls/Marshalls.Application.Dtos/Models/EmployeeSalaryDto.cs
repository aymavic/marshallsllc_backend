﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marshalls.Application.Dtos.Models
{
    public class EmployeeSalaryDto
    {
        public int Id { get; set; } 
        public string Code { get; set; }
        public string FullName { get; set; }
        public string Division { get; set; }
        public string Position { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime Birthday { get; set; }
        public string IdentificationNumber { get; set; }
        public decimal TotalSalary { get; set; }
    }
}
