﻿using Marshalls.Application.Dtos.Models;
using Marshalls.Domain.Services.Abstractions;
using Marshalls.Domain.Services.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marshalls.Application.Services.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmployeeSalaryController : ControllerBase
    {
        private readonly ILogger<EmployeeSalaryController> _logger;
        private readonly IEmployeeService _employeeService;
        public EmployeeSalaryController(ILogger<EmployeeSalaryController> logger)
        {
            _logger = logger;
            _employeeService = new EmployeeService();
        }

        [HttpGet]
        public List<EmployeeSalaryDto> GetEmployees()
        {
            return this._employeeService.GetEmployees();
        }

        [HttpGet("{id}/{type}")]
        public List<EmployeeSalaryDto> GetEmployees(int id, string type)
        {
            return this._employeeService.GetEmployeesByFilter(id, type);
        }

        [HttpGet("{code}")]
        public EmployeeBonoDto GetEmployeeSalary(string code)
        {
            return this._employeeService.GetEmployeeSalaryByCode(code);
        }
    }
}
