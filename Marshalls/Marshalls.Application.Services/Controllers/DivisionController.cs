﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Marshalls.Application.Dtos.Models;
using Marshalls.Domain.Services.Abstractions;
using Marshalls.Domain.Services.Services;

namespace Marshalls.Application.Services.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DivisionController : ControllerBase
    {
        private readonly ILogger<DivisionController> _logger;
        private readonly IDivisionService _divisionService;
        public DivisionController(ILogger<DivisionController> logger)
        {
            _logger = logger;
            _divisionService = new DivisionService();
        }

        [HttpGet]
        public List<DivisionDto> GetAll()
        {
            return this._divisionService.GetAll();
        }
    }
}
