﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Marshalls.Application.Dtos.Models;
using Marshalls.Domain.Services.Abstractions;
using Marshalls.Domain.Services.Services;

namespace Marshalls.Application.Services.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OfficeController : ControllerBase
    {
        private readonly ILogger<OfficeController> _logger;
        private readonly IOfficeService _officeService;
        public OfficeController(ILogger<OfficeController> logger)
        {
            _logger = logger;
            _officeService = new OfficeService();
        }

        [HttpGet]
        public List<OfficeDto> GetAll()
        {
            return this._officeService.GetAll();
        }
    }
}
