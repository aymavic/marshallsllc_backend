﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Marshalls.Application.Dtos.Models;
using Microsoft.AspNetCore.Components.RenderTree;
using Marshalls.Domain.Services;
using Marshalls.Domain.Services.Abstractions;
using Marshalls.Domain.Services.Services;
using Marshalls.Application.Services.JsonModels;

namespace Marshalls.Application.Services.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmployeeController : ControllerBase
    {
        private readonly ILogger<EmployeeController> _logger;
        private readonly IEmployeeService _employeeService;
        public EmployeeController(ILogger<EmployeeController> logger)
        {
            _logger = logger;
            _employeeService = new EmployeeService();
        }

        [HttpPost ("{amount}")]
        public bool GenerateEmployees(int amount)
        {
            return this._employeeService.GenerateEmployees(amount);
        }

        [HttpPost]
        public bool AddEmployees([FromBody] EmployeeJson employee)
        {
            return this._employeeService.InsertEmployees(Utils.Utils.CreateEmployeeDoms(employee));
        }

        [HttpGet("{name}/{surName}")]
        public EmployeeDto GetEmployeeByNameSurName(string name, string surName)
        {
            return this._employeeService.GetEmployeeByNameSurName(name, surName);
        }

        [HttpGet("{code}/{year}/{month}")]
        public EmployeeDto GetEmployeeByCodeYearMonth(string code, int year, int month)
        {
            return this._employeeService.GetEmployeeByCodeYearMonth(code, year, month);
        }
    }
}
