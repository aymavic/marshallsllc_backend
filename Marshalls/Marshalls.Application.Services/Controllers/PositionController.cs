﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Marshalls.Application.Dtos.Models;
using Marshalls.Domain.Services.Abstractions;
using Marshalls.Domain.Services.Services;

namespace Marshalls.Application.Services.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PositionController : ControllerBase
    {
        private readonly ILogger<PositionController> _logger;
        private readonly IPositionService _positionService;
        public PositionController(ILogger<PositionController> logger)
        {
            _logger = logger;
            _positionService = new PositionService();
        }

        [HttpGet]
        public List<PositionDto> GetAll()
        {
            return this._positionService.GetAll();
        }
    }
}
