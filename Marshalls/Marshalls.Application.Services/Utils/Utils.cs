﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Marshalls.Application.Services.JsonModels;
using Marshalls.Domain.Entities.Entities;

namespace Marshalls.Application.Services.Utils
{
    public class Utils
    {
        public static List<EmployeeDom> CreateEmployeeDoms(EmployeeJson employeeJson)
        {
            List<EmployeeDom> employees = new List<EmployeeDom>();
            EmployeeDom employee = new EmployeeDom()
            {
                EmployeeCode = employeeJson.Code,
                EmployeeName = employeeJson.Name,
                EmployeeSurname = employeeJson.Surname,
                BeginDate = employeeJson.BeginDate,
                Birthday = employeeJson.Birthday,
                IdentificationNumber = employeeJson.Identification
            };

            foreach (var salary in employeeJson.Salaries)
            {
                EmployeeDom employeeDom = employee.Clone();

                employeeDom.Id = Int32.Parse(salary.Id);
                employeeDom.Year = Int32.Parse(salary.Year);
                employeeDom.Month = Int32.Parse(salary.Month);
                employeeDom.Grade = Int32.Parse(salary.Grade);
                employeeDom.BaseSalary = Decimal.Parse(salary.BaseSalary);
                employeeDom.ProductionBonus = Decimal.Parse(salary.ProductionBonus);
                employeeDom.CompensationBonus = Decimal.Parse(salary.CompensationBonus);
                employeeDom.Commission = Decimal.Parse(salary.Commission);
                employeeDom.Contributions = Decimal.Parse(salary.Contributions);
                employeeDom.Office = new OfficeDom() { Id = Int32.Parse(salary.Office) };
                employeeDom.Division = new DivisionDom() { Id = Int32.Parse(salary.Division) };
                employeeDom.Position = new PositionDom() { Id = Int32.Parse(salary.Position) };

                employees.Add(employeeDom);
            }

            return employees;
        }
    }
}
