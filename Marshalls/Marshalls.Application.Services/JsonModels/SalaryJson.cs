﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marshalls.Application.Services.JsonModels
{
    public class SalaryJson
    {
        public string Id { get; set; }
        public string Year { get; set; }
        public string Month { get; set; }
        public string Grade { get; set; }
        public string BaseSalary { get; set; }
        public string ProductionBonus { get; set; }
        public string CompensationBonus { get; set; }
        public string Commission { get; set; }
        public string Contributions { get; set; }
        public string Office { get; set; }
        public string Division { get; set; }
        public string Position { get; set; }
    }
}
