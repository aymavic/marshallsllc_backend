﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marshalls.Application.Services.JsonModels
{
    public class EmployeeJson
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime Birthday { get; set; }
        public string Identification { get; set; }
        public IEnumerable<SalaryJson> Salaries { get; set; }
    }
}
