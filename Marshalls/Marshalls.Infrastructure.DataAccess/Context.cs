﻿using System;
using System.Collections.Generic;
using System.Text;
using Marshalls.Infrastructure.DataAccess.DBObjects;
using Microsoft.EntityFrameworkCore;
using System.Configuration;
using Microsoft.IdentityModel.Protocols;

namespace Marshalls.Infrastructure.DataAccess
{
    public class Context : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Division> Divisions { get; set; }
        public DbSet<Office> Offices { get; set; }
        public DbSet<Position> Positions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=DESKTOP-13EJH8I\MSSQLSERVER2014;Database=MarshallsDB;User ID=sa;Password=Control123;");
        }
    }
}
