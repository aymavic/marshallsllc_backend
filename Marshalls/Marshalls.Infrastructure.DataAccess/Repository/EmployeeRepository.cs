﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Marshalls.Domain.Entities.Abstractions;
using Marshalls.Domain.Entities.Entities;
using Marshalls.Infrastructure.DataAccess.DBObjects;
using Marshalls.Infrastructure.DataAccess.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Marshalls.Infrastructure.DataAccess.Repository
{
    public class EmployeeRepository : IEmployeeRepository
    {
        public int add(EmployeeDom employee)
        {
            try
            {
                using Context context = new Context();
                context.Employees.Add(Mapper.EmployeeMapper(employee));
                return context.SaveChanges();
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int add(List<EmployeeDom> employees)
        {
            try
            {
                using Context context = new Context();
                context.Employees.AddRange(Mapper.EmployeesMapper(employees));
                return context.SaveChanges();
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public IEnumerable<EmployeeDom> getEmployees(string filter)
        {
            try
            {
                using Context context = new Context();
                List<Employee> query = context.Employees.Include(d => d.Division)
                                                .Include(o => o.Office)
                                                .Include(p => p.Position).ToList<Employee>();

                return Mapper.EmployeeDomsMapper(query);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public EmployeeDom getEmployeeByNameSurName(string name, string surName)
        {
            try
            {
                using Context context = new Context();
                IEnumerable<Employee> query = context.Employees.Include(d => d.Division)
                    .Include(o => o.Office)
                    .Include(p => p.Position);
                List<Employee> filter = (from q in query
                    where q.EmployeeName == name
                    where q.EmployeeSurname == surName
                    select q).Take(1).ToList();

                if (filter.Count > 0)
                {
                    return Mapper.EmployeeDomMapper(filter[0]);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public EmployeeDom getEmployeeByCodeYearMonth(string code, int year, int month)
        {
            try
            {
                using Context context = new Context();
                IEnumerable<Employee> query = context.Employees.Include(d => d.Division)
                    .Include(o => o.Office)
                    .Include(p => p.Position);
                List<Employee> filter = (from q in query
                    where q.EmployeeCode == code
                    where q.Year == year
                    where q.Month == month
                    select q).Take(1).ToList();

                if (filter.Count > 0)
                {
                    return Mapper.EmployeeDomMapper(filter[0]);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<EmployeeDom> getEmployeesLatest()
        {
            try
            {
                List<Employee> filter = this.getEmployeesWithLastSalary();

                if (filter.Count > 0)
                {
                    return Mapper.EmployeeDomsMapper(filter);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<EmployeeDom> GetEmployeesByFilterOfficeGrade(int id)
        {
            try
            {
                List<Employee> filter = this.getEmployeesWithLastSalary();

                List<Employee> selectedEmpl = (from q in filter
                                               where q.Id == id
                                                select q).ToList();
                if (selectedEmpl.Count > 0)
                {
                    Employee selected = selectedEmpl[0];

                    List<Employee> result = (from f in filter
                        where f.IdOffice == selected.IdOffice
                        where f.Grade == selected.Grade
                        select f).ToList();

                    return Mapper.EmployeeDomsMapper(result);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<EmployeeDom> GetEmployeesByFilterOfficesGrade(int id)
        {
            try
            {
                using Context context = new Context();

                List<Employee> filter = this.getEmployeesWithLastSalary();

                List<Employee> selectedEmpl = (from q in filter
                    where q.Id == id
                    select q).ToList();
                if (selectedEmpl.Count > 0)
                {
                    Employee selected = selectedEmpl[0];

                    IEnumerable<Employee> officesEmpl = context.Employees.Include(o => o.Office);
                    List<int> officeIds = (from o in officesEmpl
                        where selected.EmployeeCode == o.EmployeeCode
                        select o.IdOffice).ToList();

                    List<Employee> result = (from f in filter
                        where officeIds.Contains(f.IdOffice)
                        where f.Grade == selected.Grade
                        select f).ToList();

                    return Mapper.EmployeeDomsMapper(result);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<EmployeeDom> GetEmployeesByFilterPositionGrade(int id)
        {
            List<Employee> filter = this.getEmployeesWithLastSalary();

            List<Employee> selectedEmpl = (from q in filter
                where q.Id == id
                select q).ToList();
            if (selectedEmpl.Count > 0)
            {
                Employee selected = selectedEmpl[0];

                List<Employee> result = (from f in filter
                    where f.IdPosition == selected.IdPosition
                    where f.Grade == selected.Grade
                    select f).ToList();

                return Mapper.EmployeeDomsMapper(result);
            }
            else
            {
                return null;
            }
        }

        public List<EmployeeDom> GetEmployeesByFilterPositionsGrade(int id)
        {
            try
            {
                using Context context = new Context();

                List<Employee> filter = this.getEmployeesWithLastSalary();

                List<Employee> selectedEmpl = (from q in filter
                    where q.Id == id
                    select q).ToList();
                if (selectedEmpl.Count > 0)
                {
                    Employee selected = selectedEmpl[0];

                    IEnumerable<Employee> positionEmpl = context.Employees.Include(o => o.Position);
                    List<int> positionIds = (from p in positionEmpl
                        where selected.EmployeeCode == p.EmployeeCode
                        select p.IdPosition).ToList();

                    List<Employee> result = (from f in filter
                        where positionIds.Contains(f.IdPosition)
                        where f.Grade == selected.Grade
                        select f).ToList();

                    return Mapper.EmployeeDomsMapper(result);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<EmployeeDom> GetEmployeeSalaryByCode(string code)
        {
            try
            {
                using Context context = new Context();

                List<Employee> employees = (from e in context.Employees.Include(d => d.Division)
                        .Include(o => o.Office)
                        .Include(p => p.Position)
                                            where e.EmployeeCode == code
                    orderby e.Year, e.Month descending 
                    select e).Take(3).ToList();

                return Mapper.EmployeeDomsMapper(employees);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private List<Employee> getEmployeesWithLastSalary()
        {
            try
            {
                using Context context = new Context();
                IEnumerable<Employee> query = context.Employees.Include(d => d.Division)
                    .Include(o => o.Office)
                    .Include(p => p.Position);

                var employees =
                    from q in query
                    group q by q.EmployeeCode into emplGroup
                    select emplGroup;

                IEnumerable<Employee> filter = (from ee in employees
                    select (from oo in ee
                        orderby oo.Year, oo.Month descending
                        select oo).Take(1).FirstOrDefault());

                return filter.ToList();
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
