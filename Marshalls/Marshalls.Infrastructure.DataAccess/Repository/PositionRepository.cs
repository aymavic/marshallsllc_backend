﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Marshalls.Domain.Entities.Abstractions;
using Marshalls.Domain.Entities.Entities;
using Marshalls.Infrastructure.DataAccess.Utils;
using Marshalls.Infrastructure.DataAccess.DBObjects;

namespace Marshalls.Infrastructure.DataAccess.Repository
{
    public class PositionRepository : IPositionRepository
    {
        public int add(PositionDom position)
        {
            try
            {
                using Context context = new Context();
                context.Positions.Add(Mapper.PositionMapper(position));
                return context.SaveChanges();
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int add(List<PositionDom> positions)
        {
            try
            {
                using Context context = new Context();
                context.Positions.AddRange(Mapper.PositionsMapper(positions));
                return context.SaveChanges();
            }
            catch (Exception e)
            {
                return -1;
            }
}

        public IEnumerable<PositionDom> getPositions(string filter)
        {
            try
            {
                using Context context = new Context();
                List<Position> query = context.Positions.ToList<Position>();

                return Mapper.PositionDomsMapper(query);
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
