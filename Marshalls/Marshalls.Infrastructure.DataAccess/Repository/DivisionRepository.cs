﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Marshalls.Domain.Entities.Abstractions;
using Marshalls.Domain.Entities.Entities;
using Marshalls.Infrastructure.DataAccess.DBObjects;
using Marshalls.Infrastructure.DataAccess.Utils;

namespace Marshalls.Infrastructure.DataAccess.Repository
{
    public class DivisionRepository : IDivisionRepository
    {
        public int add(DivisionDom division)
        {
            try
            {
                using Context context = new Context();
                context.Divisions.Add(Mapper.DivisionMapper(division));
                return context.SaveChanges();
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int add(List<DivisionDom> divisions)
        {
            try
            {
                using Context context = new Context();
                context.Divisions.AddRange(Mapper.DivisionsMapper(divisions));
                return context.SaveChanges();
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public IEnumerable<DivisionDom> getDivisions(string filter)
        {
            try
            {
                using Context context = new Context();
                List<Division> query = context.Divisions.ToList<Division>();

                return Mapper.DivisionDomsMapper(query);
            }
            catch (Exception e)
            {
                return null;
            }
            
        }
    }
}
