﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Marshalls.Domain.Entities.Abstractions;
using Marshalls.Domain.Entities.Entities;
using Marshalls.Infrastructure.DataAccess.Utils;
using Marshalls.Infrastructure.DataAccess.DBObjects;

namespace Marshalls.Infrastructure.DataAccess.Repository
{
    public class OfficeRepository : IOfficeRepository
    {
        public int add(OfficeDom office)
        {
            try
            {
                using Context context = new Context();
                context.Offices.Add(Mapper.OfficeMapper(office));
                return context.SaveChanges();
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public int add(List<OfficeDom> offices)
        {
            try
            {
                using Context context = new Context();
                context.Offices.AddRange(Mapper.OfficesMapper(offices));
                return context.SaveChanges();
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public IEnumerable<OfficeDom> getOffices(string filter)
        {
            try
            {
                using Context context = new Context();
                List<Office> query = context.Offices.ToList<Office>();

                return Mapper.OfficeDomsMapper(query);
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
