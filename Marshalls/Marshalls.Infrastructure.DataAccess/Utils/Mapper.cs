﻿using System;
using System.Collections.Generic;
using System.Text;
using Marshalls.Domain.Entities.Entities;
using Marshalls.Infrastructure.DataAccess.DBObjects;

namespace Marshalls.Infrastructure.DataAccess.Utils
{
    public class Mapper
    {
        public static Employee EmployeeMapper(EmployeeDom employeeDom)
        {
            Employee employee = new Employee()
            {
                Id = employeeDom.Id,
                Year = employeeDom.Year,
                Month = employeeDom.Month,
                EmployeeCode = employeeDom.EmployeeCode,
                EmployeeName = employeeDom.EmployeeName,
                EmployeeSurname = employeeDom.EmployeeSurname,
                Grade = employeeDom.Grade,
                BeginDate = employeeDom.BeginDate,
                Birthday = employeeDom.Birthday,
                IdentificationNumber = employeeDom.IdentificationNumber,
                BaseSalary = employeeDom.BaseSalary,
                ProductionBonus = employeeDom.ProductionBonus,
                CompensationBonus = employeeDom.CompensationBonus,
                Commission = employeeDom.Commission,
                Contributions = employeeDom.Contributions,
                IdOffice = employeeDom.Office.Id,
                //Office = Mapper.OfficeMapper(employeeDom.Office),
                IdDivision = employeeDom.Division.Id,
                //Division = Mapper.DivisionMapper(employeeDom.Division),
                IdPosition = employeeDom.Position.Id,
                //Position = Mapper.PositionMapper(employeeDom.Position)
            };

            return employee;
        }

        public static Office OfficeMapper(OfficeDom officeDom)
        {
            Office office = new Office()
            {
                Id = officeDom.Id,
                Name = officeDom.Name
            };

            return office;
        }

        public static Division DivisionMapper(DivisionDom divisionDom)
        {
            Division division = new Division()
            {
                Id = divisionDom.Id,
                Name = divisionDom.Name
            };

            return division;
        }

        public static Position PositionMapper(PositionDom positionDom)
        {
            Position position = new Position()
            {
                Id = positionDom.Id,
                Name = positionDom.Name
            };

            return position;
        }

        public static List<Employee> EmployeesMapper(List<EmployeeDom> employeeDoms)
        {
            List<Employee> employees = new List<Employee>();
            foreach (var employeeDom in employeeDoms)
            {
                employees.Add(Mapper.EmployeeMapper(employeeDom));
            }

            return employees;
        }

        public static List<Office> OfficesMapper(List<OfficeDom> officeDoms)
        {
            List<Office> offices = new List<Office>();
            foreach (var officeDom in officeDoms)
            {
                offices.Add(Mapper.OfficeMapper(officeDom));   
            }

            return offices;
        }

        public static List<Division> DivisionsMapper(List<DivisionDom> divisionDoms)
        {
            List<Division> divisions = new List<Division>();
            foreach (var divisionDom in divisionDoms)
            {
                divisions.Add(Mapper.DivisionMapper(divisionDom));
            }

            return divisions;
        }

        public static List<Position> PositionsMapper(List<PositionDom> positionDoms)
        {
            List<Position> positions = new List<Position>();
            foreach (var positionDom in positionDoms)
            {
                positions.Add(Mapper.PositionMapper(positionDom));
            }

            return positions;
        }

        public static EmployeeDom EmployeeDomMapper(Employee employee)
        {
            EmployeeDom employeeDom = new EmployeeDom()
            {
                Id = employee.Id,
                Year = employee.Year,
                Month = employee.Month,
                EmployeeCode = employee.EmployeeCode,
                EmployeeName = employee.EmployeeName,
                EmployeeSurname = employee.EmployeeSurname,
                Grade = employee.Grade,
                BeginDate = employee.BeginDate,
                Birthday = employee.Birthday,
                IdentificationNumber = employee.IdentificationNumber,
                BaseSalary = employee.BaseSalary,
                ProductionBonus = employee.ProductionBonus,
                CompensationBonus = employee.CompensationBonus,
                Commission = employee.Commission,
                Contributions = employee.Contributions,
                Office = Mapper.OfficeDomMapper(employee.Office),
                Division = Mapper.DivisionDomMapper(employee.Division),
                Position = Mapper.PositionDomMapper(employee.Position)
            };

            return employeeDom;
        }

        public static OfficeDom OfficeDomMapper(Office office)
        {
            OfficeDom officeDom = new OfficeDom()
            {
                Id = office.Id,
                Name = office.Name
            };

            return officeDom;
        }

        public static DivisionDom DivisionDomMapper(Division division)
        {
            DivisionDom divisionDom = new DivisionDom()
            {
                Id = division.Id,
                Name = division.Name
            };

            return divisionDom;
        }

        public static PositionDom PositionDomMapper(Position position)
        {
            PositionDom positionDom = new PositionDom()
            {
                Id = position.Id,
                Name = position.Name
            };

            return positionDom;
        }

        public static List<EmployeeDom> EmployeeDomsMapper(List<Employee> employees)
        {
            List<EmployeeDom> employeeDoms = new List<EmployeeDom>();
            foreach (var employee in employees)
            {
                employeeDoms.Add(Mapper.EmployeeDomMapper(employee));
            }

            return employeeDoms;
        }

        public static List<OfficeDom> OfficeDomsMapper(List<Office> offices)
        {
            List<OfficeDom> officeDoms = new List<OfficeDom>();
            foreach (var office in offices)
            {
                officeDoms.Add(Mapper.OfficeDomMapper(office));
            }

            return officeDoms;
        }

        public static List<DivisionDom> DivisionDomsMapper(List<Division> divisions)
        {
            List<DivisionDom> divisionDoms = new List<DivisionDom>();
            foreach (var division in divisions)
            {
                divisionDoms.Add(Mapper.DivisionDomMapper(division));
            }

            return divisionDoms;
        }

        public static List<PositionDom> PositionDomsMapper(List<Position> positions)
        {
            List<PositionDom> positionDoms = new List<PositionDom>();
            foreach (var position in positions)
            {
                positionDoms.Add(Mapper.PositionDomMapper(position));
            }

            return positionDoms;
        }
    }
}
