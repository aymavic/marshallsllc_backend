﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Marshalls.Infrastructure.DataAccess.DBObjects
{
    public class Employee
    {
        public int Id { get; set; }
        [Required]
        public int Year { get; set; }
        [Required]
        public int Month { get; set; }
        [Required]
        [StringLength(10)]
        public string EmployeeCode { get; set; }
        [Required]
        [StringLength(150)]
        public string EmployeeName { get; set; }
        [Required]
        [StringLength(150)]
        public string EmployeeSurname { get; set; }
        [Required]
        public int Grade { get; set; }
        [Required]
        public DateTime BeginDate { get; set; }
        [Required]
        public DateTime Birthday { get; set; }
        [Required]
        [StringLength(10)]
        public string IdentificationNumber { get; set; }
        [Required]
        public decimal BaseSalary { get; set; }
        [Required]
        public decimal ProductionBonus { get; set; }
        [Required]
        public decimal CompensationBonus { get; set; }
        [Required]
        public decimal Commission { get; set; }
        [Required]
        public decimal Contributions { get; set; }

        [ForeignKey("Office")]
        public int IdOffice { get; set; }

        [ForeignKey("IdOffice")]
        public virtual Office Office { get; set; }

        [ForeignKey("Division")]
        public int IdDivision { get; set; }

        [ForeignKey("IdDivision")]
        public virtual Division Division { get; set; }

        [ForeignKey("Position")]
        public int IdPosition { get; set; }

        [ForeignKey("IdPosition")]
        public virtual Position Position { get; set; }
    }
}
