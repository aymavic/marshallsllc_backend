﻿using System;
using System.Collections.Generic;
using System.Text;
using Marshalls.Domain.Entities.Entities;

namespace Marshalls.Domain.Entities.Abstractions
{
    public interface IDivisionRepository
    {
        int add(DivisionDom division);
        int add(List<DivisionDom> divisions);
        IEnumerable<DivisionDom> getDivisions(string filter);
         
    }
}
