﻿using System;
using System.Collections.Generic;
using System.Text;
using Marshalls.Domain.Entities.Entities;

namespace Marshalls.Domain.Entities.Abstractions
{
    public interface IPositionRepository
    {
        int add(PositionDom position);
        int add(List<PositionDom> positions);
        IEnumerable<PositionDom> getPositions(string filter);
    }
}
