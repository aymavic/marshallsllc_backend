﻿using System;
using System.Collections.Generic;
using System.Text;
using Marshalls.Domain.Entities.Entities;

namespace Marshalls.Domain.Entities.Abstractions
{
    public interface IEmployeeRepository
    {
        int add(EmployeeDom employee);
        int add(List<EmployeeDom> employees);
        IEnumerable<EmployeeDom> getEmployees(string filter);
        EmployeeDom getEmployeeByNameSurName(string name, string surName);
        EmployeeDom getEmployeeByCodeYearMonth(string code, int year, int month);
        List<EmployeeDom> getEmployeesLatest();
        List<EmployeeDom> GetEmployeesByFilterOfficeGrade(int id);
        List<EmployeeDom> GetEmployeesByFilterOfficesGrade(int id);
        List<EmployeeDom> GetEmployeesByFilterPositionGrade(int id);
        List<EmployeeDom> GetEmployeesByFilterPositionsGrade(int id);
        List<EmployeeDom> GetEmployeeSalaryByCode(string code);
    }
}
