﻿using System;
using System.Collections.Generic;
using System.Text;
using Marshalls.Domain.Entities.Entities;

namespace Marshalls.Domain.Entities.Abstractions
{
    public interface IOfficeRepository
    {
        int add(OfficeDom office);
        int add(List<OfficeDom> offices);
        IEnumerable<OfficeDom> getOffices(string filter);
    }
}
