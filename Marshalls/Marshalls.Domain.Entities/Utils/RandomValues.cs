﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Marshalls.Domain.Entities.Utils
{
    public class RandomValues
    {
        public static string RandomString(Random random, int length)
        {
            string characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string word = "";

            for (int i = 0; i < length; i++)
            {
                word += characters[random.Next(characters.Length)].ToString();
            }

            return word;
        }

        public static DateTime RandomDay(Random random, int yearStart, int yearEnd)
        {
            DateTime start = new DateTime(yearStart, 1, 1);
            int range = (DateTime.Today - start).Days - (yearEnd * 365);
            return start.AddDays(random.Next(range));
        }
    }
}
