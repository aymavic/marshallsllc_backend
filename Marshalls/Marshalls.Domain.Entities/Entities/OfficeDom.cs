﻿using Marshalls.Domain.Entities.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marshalls.Domain.Entities.Entities
{
    public class OfficeDom
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public void AssignRandomValues()
        {
            Random random = new Random();

            this.Id = 0;
            this.Name = RandomValues.RandomString(random, random.Next(1, 3));
        }
    }
}
