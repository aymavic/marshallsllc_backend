﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Marshalls.Domain.Entities.Utils;

namespace Marshalls.Domain.Entities.Entities
{
    public class EmployeeDom
    {
        public int Id { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeSurname { get; set; }
        public int Grade { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime Birthday { get; set; }
        public string IdentificationNumber { get; set; }
        public decimal BaseSalary { get; set; }
        public decimal ProductionBonus { get; set; }
        public decimal CompensationBonus { get; set; }
        public decimal Commission { get; set; }
        public decimal Contributions { get; set; }
        public OfficeDom Office { get; set; }
        public DivisionDom Division { get; set; }
        public PositionDom Position { get; set; }

        public EmployeeDom Clone()
        {
            EmployeeDom employee = new EmployeeDom()
            {
                Id = this.Id,
                EmployeeCode = this.EmployeeCode,
                EmployeeName = this.EmployeeName,
                EmployeeSurname = this.EmployeeSurname,
                Birthday = this.Birthday,
                BeginDate = this.BeginDate,
                IdentificationNumber = this.IdentificationNumber,
                Grade = this.Grade,
                BaseSalary = this.BaseSalary,
                Year = this.Year,
                Month = this.Month
            };

            return employee;
        }

        public void AssignRandomValues()
        {
            Random random = new Random();

            this.Id = 0;
            this.EmployeeCode = random.Next(2000000, 3000000).ToString();
            this.EmployeeName = RandomValues.RandomString(random, random.Next(7, 15));
            this.EmployeeSurname = RandomValues.RandomString(random, random.Next(7, 15));
            this.Birthday = RandomValues.RandomDay(random, 1961, 22);
            this.BeginDate = RandomValues.RandomDay(random, this.Birthday.Year + 18, 3);
            this.IdentificationNumber = random.Next(2000000, 50000000).ToString();
            this.Grade = random.Next(5, 20);
            this.BaseSalary = random.Next(250000, 1500000) / 100; 
            this.Year = 2020;
            this.Month = random.Next(1, 3);
        }

        public void AssignVariableValues()
        {
            Random random = new Random();
            this.Grade = random.Next(0, 10) > 5 ? this.Grade : random.Next(5, 20);
            this.BaseSalary = random.Next(0, 10) > 5 ? random.Next(250000, 1500000) / 100 : this.BaseSalary;

            ProductionBonus = random.Next(0, 400000) / 100;
            CompensationBonus = random.Next(0, 350000) / 100;
            Commission = random.Next(0, 500000) / 100;
            Contributions = random.Next(0, 100000) / 100;
        }

        public void AssignCorrelativeValues()
        {
            Random random = new Random();
            this.Month += random.Next(0, 10) > 5 ? 1 : random.Next(1, 3);

            if (Month > 12)
            {
                this.Month = this.Month - 12;
                this.Year++;
            }
        }
    }
}
