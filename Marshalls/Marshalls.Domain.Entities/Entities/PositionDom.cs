﻿using Marshalls.Domain.Entities.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marshalls.Domain.Entities.Entities
{
    public class PositionDom
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public void AssignRandomValues()
        {
            Random random = new Random();

            this.Id = 0;
            this.Name = RandomValues.RandomString(random, random.Next(5, 6)) + " " + RandomValues.RandomString(random, random.Next(5, 7));
        }
    }
}
