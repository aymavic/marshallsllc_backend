﻿using System;
using System.Collections.Generic;
using System.Text;
using Marshalls.Application.Dtos.Models;
using Marshalls.Domain.Entities.Abstractions;
using Marshalls.Domain.Entities.Entities;
using Marshalls.Domain.Services.Abstractions;
using Marshalls.Domain.Services.Utils;
using Marshalls.Infrastructure.DataAccess.Repository;

namespace Marshalls.Domain.Services.Services
{
    public class PositionService : IPositionService
    {
        private readonly IPositionRepository _positionRepository;

        public PositionService()
        {
            _positionRepository = new PositionRepository();
        }

        public List<PositionDto> GetAll()
        {
            IEnumerable<PositionDom> positionDoms = _positionRepository.getPositions("");
            return Mapper.PositionDtosMapper(positionDoms);
        }
    }
}
