﻿using System;
using System.Collections.Generic;
using System.Text;
using Marshalls.Application.Dtos.Models;
using Marshalls.Domain.Entities.Abstractions;
using Marshalls.Domain.Entities.Entities;
using Marshalls.Domain.Services.Abstractions;
using Marshalls.Domain.Services.Utils;
using Marshalls.Infrastructure.DataAccess.Repository;

namespace Marshalls.Domain.Services.Services
{
    public class OfficeService : IOfficeService
    {
        private readonly IOfficeRepository _officeRepository;

        public OfficeService()
        {
            _officeRepository = new OfficeRepository();
        }

        public List<OfficeDto> GetAll()
        {
            IEnumerable<OfficeDom> officeDoms = _officeRepository.getOffices("");
            return Mapper.OfficeDtosMapper(officeDoms);
        }
    }
}
