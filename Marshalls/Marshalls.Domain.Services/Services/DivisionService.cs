﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Marshalls.Application.Dtos.Models;
using Marshalls.Domain.Entities.Abstractions;
using Marshalls.Domain.Entities.Entities;
using Marshalls.Domain.Services.Abstractions;
using Marshalls.Domain.Services.Utils;
using Marshalls.Infrastructure.DataAccess.Repository;

namespace Marshalls.Domain.Services.Services
{
    public class DivisionService : IDivisionService
    {
        private readonly IDivisionRepository _divisionRepository;

        public DivisionService()
        {
            _divisionRepository = new DivisionRepository();
        }

        public List<DivisionDto> GetAll()
        {
            IEnumerable<DivisionDom> result = _divisionRepository.getDivisions("");

            return Mapper.DivisionDtosMapper(result);
        }
    }
}
