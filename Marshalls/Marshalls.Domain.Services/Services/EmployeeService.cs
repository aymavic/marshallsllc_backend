﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Marshalls.Application.Dtos.Models;
using Marshalls.Domain.Entities.Abstractions;
using Marshalls.Domain.Entities.Entities;
using Marshalls.Domain.Services.Abstractions;
using Marshalls.Domain.Services.Utils;
using Marshalls.Infrastructure.DataAccess.Repository;

namespace Marshalls.Domain.Services.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeService()
        {
            _employeeRepository = new EmployeeRepository();
        }

        public bool GenerateEmployees(int amount)
        {
            Seeder seeder = new Seeder();
            seeder.InsertData(amount);
            return _employeeRepository.getEmployees("").ToString().Length > 0;
        }

        public bool InsertEmployees(List<EmployeeDom> employees)
        {
            return _employeeRepository.add(employees) == 0 ? true : false;
        }

        public EmployeeDto GetEmployeeByNameSurName(string name, string surName)
        {
            EmployeeDom employee= _employeeRepository.getEmployeeByNameSurName(name, surName);

            if (employee != null)
            {
                return Mapper.EmployeeDtoMapper(employee);
            }
            else
            {
                return null;
            }
        }

        public EmployeeDto GetEmployeeByCodeYearMonth(string code, int year, int month)
        {
            EmployeeDom employee = _employeeRepository.getEmployeeByCodeYearMonth(code, year, month);

            if (employee != null)
            {
                return Mapper.EmployeeDtoMapper(employee);
            }
            else
            {
                return null;
            }
        }

        public List<EmployeeSalaryDto> GetEmployees()
        {
            List<EmployeeDom> employees = _employeeRepository.getEmployeesLatest();

            return Mapper.EmployeeSalaryDtosMapper(employees);
        }

        public List<EmployeeSalaryDto> GetEmployeesByFilter(int id, string type)
        {
            List<EmployeeDom> employees = new List<EmployeeDom>();
            switch (type)
            {
                case "OffGra":
                    employees = _employeeRepository.GetEmployeesByFilterOfficeGrade(id);
                    break;
                case "OffsGra":
                    employees = _employeeRepository.GetEmployeesByFilterOfficesGrade(id);
                    break;
                case "PosGra":
                    employees = _employeeRepository.GetEmployeesByFilterPositionGrade(id);
                    break;
                case "PossGra":
                    employees = _employeeRepository.GetEmployeesByFilterPositionsGrade(id);
                    break;
                default:
                    break;
            }

            return Mapper.EmployeeSalaryDtosMapper(employees);
        }

        public EmployeeBonoDto GetEmployeeSalaryByCode(string code)
        {
            List<EmployeeDom> employees = _employeeRepository.GetEmployeeSalaryByCode(code);

            if (employees.Count > 0)
            {
                EmployeeBonoDto employeeBono = new EmployeeBonoDto()
                {
                    Code = code,
                    FullName = string.Format("{0} {1}",employees[0].EmployeeName, employees[0].EmployeeSurname),
                    Year = employees[0].Year,
                    Month = employees[0].Month,
                    Salary1 = Utils.Utils.CalculateSalary(employees[0])
                };

                employeeBono.Salary2 = employees.Count > 1 ? Utils.Utils.GetSalaryIfNext(employees[0], employees[1]) : 0;
                employeeBono.Salary3 = employees.Count > 2 && employeeBono.Salary2 != 0 ? Utils.Utils.GetSalaryIfNext(employees[1], employees[2]) : 0;

                employeeBono.Bono = (employeeBono.Salary1 + employeeBono.Salary2 + employeeBono.Salary3) / 3;

                return employeeBono;
            }
            else
            {
                return null;
            }

        }
    }
}
