﻿using System;
using System.Collections.Generic;
using System.Text;
using Marshalls.Application.Dtos.Models;
using Marshalls.Domain.Entities.Entities;

namespace Marshalls.Domain.Services.Utils
{
    public class Mapper
    {
        public static EmployeeDto EmployeeDtoMapper(EmployeeDom employeeDom)
        {
            EmployeeDto employee = new EmployeeDto()
            {
                Id = employeeDom.Id,
                Year = employeeDom.Year,
                Month = employeeDom.Month,
                EmployeeCode = employeeDom.EmployeeCode,
                EmployeeName = employeeDom.EmployeeName,
                EmployeeSurname = employeeDom.EmployeeSurname,
                Grade = employeeDom.Grade,
                BeginDate = employeeDom.BeginDate,
                Birthday = employeeDom.Birthday,
                IdentificationNumber = employeeDom.IdentificationNumber,
                BaseSalary = employeeDom.BaseSalary,
                ProductionBonus = employeeDom.ProductionBonus,
                CompensationBonus = employeeDom.CompensationBonus,
                Commission = employeeDom.Commission,
                Contributions = employeeDom.Contributions,
                Office = employeeDom.Office.Name,
                Division = employeeDom.Division.Name,
                Position = employeeDom.Position.Name
            };

            return employee;
        }
        public static DivisionDto DivisionDtoMapper(DivisionDom division)
        {
            DivisionDto divisionDto = new DivisionDto()
            {
                Id = division.Id,
                Name = division.Name
            };

            return divisionDto;
        }

        public static OfficeDto OfficeDtoMapper(OfficeDom office)
        {
            OfficeDto officeDto = new OfficeDto()
            {
                Id = office.Id,
                Name = office.Name
            };

            return officeDto;
        }

        public static PositionDto PositionDtoMapper(PositionDom position)
        {
            PositionDto positionDto = new PositionDto()
            {
                Id = position.Id,
                Name = position.Name
            };

            return positionDto;
        }

        public static EmployeeSalaryDto EmployeeSalaryDtoMapper(EmployeeDom employee)
        {
            EmployeeSalaryDto employeeSalaryDto = new EmployeeSalaryDto()
            {
                Id = employee.Id,
                Code = employee.EmployeeCode,
                FullName = string.Format("{0} {1}", employee.EmployeeName, employee.EmployeeSurname),
                Division = employee.Division.Name,
                Position = employee.Position.Name,
                BeginDate = employee.BeginDate,
                Birthday = employee.Birthday,
                IdentificationNumber = employee.IdentificationNumber,
                TotalSalary = Utils.CalculateSalary(employee)
            };

            return employeeSalaryDto;
        }

        public static List<EmployeeDto> EmployeeDtosMapper(IEnumerable<EmployeeDom> employees)
        {
            List<EmployeeDto> employeeDtos = new List<EmployeeDto>();
            foreach (var employee in employees)
            {
                employeeDtos.Add(Mapper.EmployeeDtoMapper(employee));
            }

            return employeeDtos;
        }

        public static List<DivisionDto> DivisionDtosMapper(IEnumerable<DivisionDom> divisions)
        {
            List<DivisionDto> divisionDtos = new List<DivisionDto>();
            foreach (var division in divisions)
            {
                divisionDtos.Add(Mapper.DivisionDtoMapper(division));
            }

            return divisionDtos;
        }

        public static List<OfficeDto> OfficeDtosMapper(IEnumerable<OfficeDom> offices)
        {
            List<OfficeDto> officeDtos = new List<OfficeDto>();
            foreach (var office in offices)
            {
                officeDtos.Add(Mapper.OfficeDtoMapper(office));
            }

            return officeDtos;
        }

        public static List<PositionDto> PositionDtosMapper(IEnumerable<PositionDom> positions)
        {
            List<PositionDto> positionDtos = new List<PositionDto>();
            foreach (var position in positions)
            {
                positionDtos.Add(Mapper.PositionDtoMapper(position));
            }

            return positionDtos;
        }

        public static List<EmployeeSalaryDto> EmployeeSalaryDtosMapper(List<EmployeeDom> employees)
        {
            List<EmployeeSalaryDto> employeeSalaryDtos = new List<EmployeeSalaryDto>();
            foreach (var employee in employees)
            {
                employeeSalaryDtos.Add(Mapper.EmployeeSalaryDtoMapper(employee));
            }

            return employeeSalaryDtos;
        }
    }
}
