﻿using Marshalls.Domain.Entities.Abstractions;
using Marshalls.Domain.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Marshalls.Infrastructure.DataAccess.Repository;


namespace Marshalls.Domain.Services.Utils
{
    public class Seeder
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IDivisionRepository _divisionRepository;
        private readonly IOfficeRepository _officeRepository;
        private readonly IPositionRepository _positionRepository;

        public Seeder()
        {
            _employeeRepository = new EmployeeRepository();
            _divisionRepository = new DivisionRepository();
            _officeRepository = new OfficeRepository();
            _positionRepository = new PositionRepository();
        }

        public void InsertData(int amount)
        {
            this.InsertDivisions();
            this.InsertOffices();
            this.InsertPositions();

            IEnumerable<DivisionDom> divisions = _divisionRepository.getDivisions("");
            IEnumerable<OfficeDom> offices = _officeRepository.getOffices("");
            IEnumerable<PositionDom> positions = _positionRepository.getPositions("");

            this.InsertEmployees(amount, divisions.ToList<DivisionDom>(), offices.ToList<OfficeDom>(),
                positions.ToList<PositionDom>());
        }

        public void InsertEmployees(int amount, List<DivisionDom> divisions, List<OfficeDom> offices, List<PositionDom> positions)
        {
            List<EmployeeDom> employeeDoms = new List<EmployeeDom>();
            Random random = new Random();

            for (int i = 0; i < amount; i++)
            {
                EmployeeDom employee = new EmployeeDom();
                employee.AssignRandomValues();
                employee.AssignVariableValues();
                employee.Division = divisions[random.Next(0, divisions.Count - 1)];
                employee.Office = offices[random.Next(0, offices.Count - 1)];
                employee.Position = positions[random.Next(0, positions.Count - 1)];

                employeeDoms.Add(employee);

                for (int j = 0; j < 5; j++)
                {
                    EmployeeDom employeeDom = employee.Clone();
                    employeeDom.AssignVariableValues();
                    employeeDom.AssignCorrelativeValues();
                    employeeDom.Division = divisions[random.Next(0, divisions.Count - 1)];
                    employeeDom.Office = offices[random.Next(0, offices.Count - 1)];
                    employeeDom.Position = positions[random.Next(0, positions.Count - 1)];

                    employeeDoms.Add(employeeDom);
                    employee = employeeDom;
                }

            }

            _employeeRepository.add(employeeDoms);
        }

        public void InsertDivisions()
        {
            List<DivisionDom> divisionDoms = new List<DivisionDom>();
            for (int i = 0; i < 5; i++)
            {
                DivisionDom divisionDom = new DivisionDom();
                divisionDom.AssignRandomValues();

                divisionDoms.Add(divisionDom);
            }

            _divisionRepository.add(divisionDoms);
        }

        public void InsertOffices()
        {
            List<OfficeDom> officeDoms = new List<OfficeDom>();
            for (int i = 0; i < 5; i++)
            {
                OfficeDom officeDom = new OfficeDom();
                officeDom.AssignRandomValues();

                officeDoms.Add(officeDom);
            }

            _officeRepository.add(officeDoms);
        }

        public void InsertPositions()
        {
            List<PositionDom> positionDoms = new List<PositionDom>();
            for (int i = 0; i < 5; i++)
            {
                PositionDom positionDom = new PositionDom();
                positionDom.AssignRandomValues();

                positionDoms.Add(positionDom);
            }

            _positionRepository.add(positionDoms);
        }
    }
}
