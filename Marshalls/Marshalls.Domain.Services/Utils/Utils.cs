﻿using System;
using System.Collections.Generic;
using System.Text;
using Marshalls.Domain.Entities.Entities;

namespace Marshalls.Domain.Services.Utils
{
    public class Utils
    {
        public static decimal CalculateSalary(EmployeeDom employee)
        {
            decimal otherIncome = ((employee.BaseSalary + employee.Commission) * (decimal)0.08) + employee.Commission;
            return employee.BaseSalary + employee.ProductionBonus + (employee.CompensationBonus * (decimal) 0.75) +
                   otherIncome;
        }

        public static decimal GetSalaryIfNext(EmployeeDom first, EmployeeDom secund)
        {
            if (first.Month == (secund.Month + 1 > 12 ? 1 : secund.Month + 1) &&
                (first.Year == secund.Year || (first.Year > secund.Year && secund.Month == 12)))
            {
                return Utils.CalculateSalary(secund);
            }

            return 0;
        }
    }
}
