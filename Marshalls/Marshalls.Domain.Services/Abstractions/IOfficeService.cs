﻿using System;
using System.Collections.Generic;
using System.Text;
using Marshalls.Application.Dtos.Models;

namespace Marshalls.Domain.Services.Abstractions
{
    public interface IOfficeService
    {
        List<OfficeDto> GetAll();
    }
}
