﻿using System;
using System.Collections.Generic;
using System.Text;
using Marshalls.Application.Dtos.Models;
using Marshalls.Domain.Entities.Entities;

namespace Marshalls.Domain.Services.Abstractions
{
    public interface IEmployeeService
    {
        bool GenerateEmployees(int amount);
        bool InsertEmployees(List<EmployeeDom> employees);

        EmployeeDto GetEmployeeByNameSurName(string name, string surName);
        EmployeeDto GetEmployeeByCodeYearMonth(string code, int year, int month);
        List<EmployeeSalaryDto> GetEmployees();

        List<EmployeeSalaryDto> GetEmployeesByFilter(int id, string type);
        EmployeeBonoDto GetEmployeeSalaryByCode(string code);
    }
}
